﻿using System;
using Microsoft.EntityFrameworkCore;
using MVCWS.Models;


namespace MVCWS.Data
{
    public class MvcMovieContext : DbContext
    {
        public MvcMovieContext(DbContextOptions<MvcMovieContext> options) : base(options)
        {

        }

        public DbSet<Movie> Movie { get; set; }
        public DbSet<Menber> Menber { get; set; }
    }
       
}
