﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCWS.Data;
using MVCWS.Models;

namespace MVCWS.Controllers
{
    public class HomeController : Controller
    {
        private readonly MvcMovieContext _context;

        public HomeController(MvcMovieContext context)
        {
            _context = context;
        }

        public IActionResult Index(string username , string password)
        {

            return View();     
        }

        public IActionResult Login(string username, string password)
        {
            var data = _context.Menber.Where(m => m.username.Equals(username) && m.password.Equals(password)).SingleOrDefault();

            if (data != null)
            {
                return RedirectToAction("Index", "Movie");
            }
            return RedirectToAction("Index");


        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
